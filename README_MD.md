Perform molecular dynamics (MD) simulations
=========================================

In this tutorial, we will use QE to perform MD for a simple metallic system. 

## MD input for QE

Structural relaxation is actually MD at 0K. So some flags in MD is sort of similar to relaxation, i.e. related to atomic movement. In this example, we will use a 32 atom Au cubic supercell to simulate temperature effect to Au structure. 

MD has different types: NVE (total energy and volume are fixed), NVT (total volume and temperature are fixed), NPT (pressure and temperature are fixed). Depending on different situations, one ensumble (NVT, NPT, NVE) is chosen. For this practice, since we want to monitor the structural change under different temperatures, NVT will be used in order to setup the temperature of the system to be around A value. 

Here is the input for QE MD (in_md):

```
&control
   calculation = 'md'                 # use md 
   restart_mode='from_scratch',
   prefix='silicon',
   tstress = .true.
   tprnfor = .true.
   pseudo_dir = './'
   outdir='./'

   dt = 40d0,                         # important, setup the time-length for one MD step. Usually, this time step needs convergence test
   nstep = 100,                       # let's run 100 steps for illustration
/
&system
   ibrav=  0,
   celldm(1) =1.8897259886d0,
   nat=  32,
   ntyp= 1,
   ecutwfc =50.0,
   nspin = 1,
   occupations = 'smearing',
   smearing = 'mv',
   degauss = 0.01,

/
&electrons
   mixing_mode = 'plain'
   mixing_beta = 0.7
   conv_thr =  1.0d-6
/

&ions
   ion_dynamics = 'verlet',
   ion_temperature = 'rescaling',
   tempw = 300d0,                      # set the target temperature to be at 300K
   tolp = 50d0,
/

ATOMIC_SPECIES
  Au   196.97  ONCV.PWM.Au.UPF         # remember to setup the correct atom mass

K_POINTS Gamma                         # here, for simplicity, let's use single kpoint without testing its convergence

CELL_PARAMETERS
8.16000000  0.00000000  0.00000000
0.00000000  8.16000000  0.00000000
0.00000000  0.00000000  8.16000000

ATOMIC_POSITIONS (crystal)
Au    0.000000    0.000000    0.000000
Au    0.250000    0.000000    0.250000
Au    0.000000    0.250000    0.250000
Au    0.250000    0.250000    0.000000
Au    0.000000    0.000000    0.500000
Au    0.250000    0.000000    0.750000
Au    0.000000    0.250000    0.750000
Au    0.250000    0.250000    0.500000
Au    0.000000    0.500000    0.000000
Au    0.250000    0.500000    0.250000
Au    0.000000    0.750000    0.250000
Au    0.250000    0.750000    0.000000
Au    0.000000    0.500000    0.500000
Au    0.250000    0.500000    0.750000
Au    0.000000    0.750000    0.750000
Au    0.250000    0.750000    0.500000
Au    0.500000    0.000000    0.000000
Au    0.750000    0.000000    0.250000
Au    0.500000    0.250000    0.250000
Au    0.750000    0.250000    0.000000
Au    0.500000    0.000000    0.500000
Au    0.750000    0.000000    0.750000
Au    0.500000    0.250000    0.750000
Au    0.750000    0.250000    0.500000
Au    0.500000    0.500000    0.000000
Au    0.750000    0.500000    0.250000
Au    0.500000    0.750000    0.250000
Au    0.750000    0.750000    0.000000
Au    0.500000    0.500000    0.500000
Au    0.750000    0.500000    0.750000
Au    0.500000    0.750000    0.750000
Au    0.750000    0.750000    0.500000
```

**Note**: check the structure before you run. Also, **dt** is in atomic unit!!! (1 a.u. = 4.8378x10^-17 s)

1. use 16 core (such as node=1:ppn=16) to run as pw.x -npool 1 -ndiag 16  < in_md > out_md at 300K for 100 steps. After it finishes, plot the system temperature as a function of time. Also use VESTA to check what the stucture looks like by plotting e.g. the last MD step structure.

2. change temperature to 800K and change 'restart_mode' to 'restart', run for another 100 steps. (Here, since restart is used, the new 800K calculation will read the structure and velocity of the last step of 300K.). After it finishes, plot the system temperature as a function of time. Look at the structure of the last MD step to see anything difference.

3. change temperature to 1600K and run for 400 steps. plot the system temperature as a function of time and check the structure change.

4. Now, let's decrease the temperature slowly to room temp. to simulate a process called **"simulated annealing"** (increase to high temperature and then decrease slowly). Usually, people use this procedure to obtain the *global minimum* for a complex structure. Run at 1400K for 100 step ==> 1100K for 100 step ==>  800K for 100 steps ==> 500K for 100 steps ==> 300K for 100 steps. For accurate calculation, the temperature is decreased even slower and for one temperature range, longer simulation is performed. Here, we only run this job for illustration. 

The temperature usually oscillates greatly (such as +/- 50K or even higher). But when the system is in equilibrium, the **average** temperature should be the same to what you setup. The system will take a while before it reaches equilibrium (e.g. tens of steps, some systems may need hundreds of steps). Usually, the trajectories to reach equilibrium should be disposed for statistical analysis since they are not *equilibrium* yet.
