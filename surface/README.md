DFT calculation on a surface
=========================================

In this tutorial, we will use QE to study adsorption on a metal surface and chemical reaction simulations.

## Slab model

Surfaces or interfaces do not have translational symmetry for the direction normal to the surface (or interface). To model such 2D-type system, a **large enough vacuum space** is usually used, i.e. in out-of-plane direction of the 2D slab, there is vacuum to vanish the slab-slab interaction to mimic the broken translational symmetry; in-plane direction of the 2D slab is still similar to bulk case without any vacuum. Usually, the size of the vacuum (more specifically its height) needs a convergence test. However, slab calculation is mostly expensive owing to the vacuum space. We may want to have a balance between computational accuracy and cost. 

Similarly, we know how to similar a 1D system: vacuum in x-y directions, but bulk-like along z direction. For 0D system, such as simulating a molecule, simply put this molecule in a large enough box with vacuum in all x, y and z directions of this molecule to avoid image-image ineractions. 

![An example for 4 layer copper slab model](suf1.png)

The above figure display a **4-layer Cu slab** model. The vacuum height is around **15 Angstrom** from top layer Cu to bottom Cu layer of the image above. What the figure shows is a **[111] direction** surface. Which surface should be studied is a science and an art question depending on if this surface is stable and if it is interesting enough for investigation. Cu [111] is famous for CO2 reduction. In this example, we will study CO on Cu surface, which is one important step of CO2 reduction. Meanwhile, number of metal layers may also need convergence test. For a balance of computational cost and accuracy, we use 4 layers for now. 4 layers metal may be ok for most catalytic studies, but there are also many exceptions which needs more layers. Be careful for this!!!


## CO adsorption energy

The molecular **adsorption energy** E_ads is computed as:

    E_ads(CO) = E_tot(CO+Cu_surf) - E_tot(CO) - E_tot(Cu_surf)

i.e. it is the energy difference between DFT computed total energy of combined system CO adsorbed on Cu and total energy of pure CO plus total energy of pure Cu surface. Every system in the above equation needs a relaxation, since when CO is adsorbed on Cu surface, both CO and a few atoms of Cu near adsorption site could change their structures slightly. 

Following is the input for Cu surface relaxation (in_surf):

    &control                         
       calculation = 'relax' 
       restart_mode='from_scratch',     
       prefix='Cu111',                
       tstress = .true.                 
       tprnfor = .true.                 
       pseudo_dir = './'                
       forc_conv_thr = 0.001d0,   
       nstep = 2000,              
       outdir='./'                      
    /
    &system
       ibrav=  0,                    
       celldm(1) =1.8897259886d0,    
       nat=  64,
       ntyp= 1,                      
       ecutwfc =50.0,
       nspin = 1,                    
       occupations = 'smearing',        
       smearing = 'm-v',
       degauss = 0.001,
       input_dft = 'rpbe',
    /
    &electrons
       mixing_mode = 'plain'
       mixing_beta = 0.7
       conv_thr =  1.0d-6
    /
    
    &ions
    
    /
    
    ATOMIC_SPECIES
      Cu   28.08  Cu.SG15.PBE.UPF
    
    K_POINTS {automatic}             
     2  2  1  0  0  0
    
    CELL_PARAMETERS {alat}
       0.1056716000E+02    0.0000000000E+00    0.0000000000E+00
      -0.5283580000E+01    0.9151429010E+01    0.0000000000E+00
       0.0000000000E+00    0.0000000000E+00    0.2074720000E+02
    
    ATOMIC_POSITIONS (crystal)
      Cu    0.000000000    0.000000000    0.000000000     0  0  0
      Cu    0.081430145    0.165750026    0.105099004     1  1  1
      Cu    0.168652192    0.085279577    0.195119332     1  1  1
      Cu    0.000149236    0.999480734    0.300040853     1  1  1
      Cu    0.000000000    0.250000000    0.000000000     0  0  0
      Cu    0.081945112    0.415961989    0.105129350     1  1  1
      Cu    0.166418987    0.333237688    0.195523037     1  1  1
      ... ...
      ... ...

- integers after atomic coordinates (0  0  0 or 1  1  1) specify if this atom will move (along x, y, z) during relaxation. 0: do not move; 1: move. 

- For slab systems, to avoid the whole structure shifting along z, the few bottom layers are to be fixed during the relaxation (or MD simulation). Here, we fix the bottom layer of Cu. 

- For k point sampling, usually a single k point is sampled along vacuum direction. 

- Different from the semiconductors we have practices before, a smearing method is usually used for metal to help convergence.

Meanwhile, it is found that PBE cannot predict correct CO adsorption site. Throughout this practice, we will use 'rPBE' see [ref](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.59.7413). In real CO2 reduction reactions (CO2RR), solvent is actually quite important (including water and cations disolved in water). For this practice, we will only do the calculation in vacuum as an illustration. 

Now, we put CO on top of one of Cu atoms on the surface to build up an adsorption system (carbon is attached to Cu atom, oxygen is on top of carbon). Then we relax the whole system (fix bottom layer) using in_CO-on-surf. Here, the adsorption site of CO on Cu is another research topic. Now, some experiments have shown that CO tend to adsorb on top of Cu atoms (at least gas phase expt.). But if there is no expt. input, we may have to examing all possible sites for adsorption and compare which site provides the most stable adsorption (which has the lowest adsorption energy). 

Finally, we simulate CO in a box. For simplicity, we will use the same box of the above simulations by removing the coordinates of Cu atoms. 

All the above calculations are much more expensive than the bulk case. Be patient! After the relaxation, extracting the total energy for the relaxed systems and find out the adsorption energy! 


## CO adsorption reaction barrier

CO adsorption on Cu can be treated as a chemical reaction. For this reaction, the **reactant** is that CO is far away from Cu surface; the **final product** is that CO is adsorbed on the Cu surface as what we obtained above. Thus as a raction, it should show a **reaction barrier**. In this practice, we will use QE to simulate a chemical reaction scenario and compute the reaction barrier. 

In this case, a special relaxation technique "**Nudge elastic bands (NEB)**" is used. NEB is almost a standard way to compute chemical reaction barriers. Of course, MD simulation could also be used to obtain the value. NEB relaxes the coordinates perpendicular to the reaction path but sample the images in an even distance along the reaction path to yield the energies along the reaction. The reaction barrier could be obtained from such energy path.

NEB in QE needs to specify the reaction initial structure (reactants), intermediates, and final structure (products). Here, the intermediate images are linear interpolations between initial and final structure. They are not what we usually mean "intermediate structures". They are simply a starting path to make the program on track from the begenning. Considering the computational cost, here, we sample 7 images (including initial and final) along the path. More images will generate a more smooth energy curve but of course with more time needed.

See in_neb to get a clue for its format. Now, following the example of coordinates in in_neb, use the pure Cu and far away CO as the initial structure (let's have the CO around 8 Angstrom above top Cu surface. Think about how to make the initial stucture: you could use the pure Cu surf structure you have relaxed, also put the purely relaxed CO structure on a 8-Angstrom-position above Cu.), the CO adsorbed on Cu as the final structure, make a linear interpolation from initial to final structures at six even intermediate images. In total you have two structures (one initial plus one final) and five intermediate images.

This calculation will be long and you may have to restart several times to finish the calculation (change PATH namelist restart_mode to 'restart').

![Seven images for NEB initial set up](surf2.png)

After the relaxation converges, plot the total energy of all 7 images to find out the reaction barrier (energy difference between transition state and initial reactants).
